"use strict";

// Промежуточная аттестация

let countCart = document.querySelector(".header__button-count");
let addCart = document.querySelector(".price__button");

const productId = document.querySelector('[name="product-id"]');

document.addEventListener("DOMContentLoaded", () => {
  let cart = [];

  if (localStorage.getItem("goods") > 0) {
    cart = localStorage.getItem("goods").split(",");
    countCart.textContent = cart.length;
  }

  if (cart.length === 0) {
    countCart.textContent = "";
  }
  if (cart.length > 0) {
    addCart.textContent = "Товар уже в корзине";
    addCart.style.background = "#888888";
  }

  function addToCart(e) {
    if (!cart.includes(e)) {
      cart.push(e);
      localStorage.setItem("goods", cart);
      countCart.textContent = cart.length;
    }
  }

  function removeFromCart(e) {
    let i = cart.indexOf(e);
    if (i !== -1) {
      cart.splice(i, 1);
      localStorage.setItem("goods", cart);
      countCart.textContent = cart.length;
      if (localStorage.getItem("goods") === "") {
        localStorage.removeItem("goods");
      }
      if (cart.length === 0) {
        countCart.textContent = "";
      }
    }
  }

  addCart.addEventListener("click", () => {
    if (cart.length > 0) {
      addCart.textContent = "Добавить в корзину";
      addCart.style.background = "#f36223";
      removeFromCart(productId.value);
    } else {
      addCart.textContent = "Товар уже в корзине";
      addCart.style.background = "#888888";
      addToCart(productId.value);
    }
  });
});

// Конец промежуточной аттестации

let form = document.querySelector("form");
let inputNameContainer = form.querySelector(".input-name");
let inputScoreContainer = form.querySelector(".input-score");

let inputName = inputNameContainer.querySelector(".review__form-name");
let errorNameElem = inputNameContainer.querySelector(".error-name");

let inputScore = inputScoreContainer.querySelector(".review__form-rate");
let errorNameScore = inputScoreContainer.querySelector(".error-rate");

function handleFormSubmit(event) {
  event.preventDefault();

  let name = inputName.value;
  let score = +inputScore.value;

  let errorName = "";

  if (name.length < 2) {
    errorName = "Имя не может быть короче 2-х символов";
  }

  if (name.length === 0) {
    errorName = "Вы забыли указать имя и фамилию";
  }

  if (errorName) {
    errorNameElem.innerText = errorName;
    errorNameElem.style.opacity = 1;
  } else {
    errorNameElem.style.opacity = 0;
  }

  // For Rating

  let errorScore = "";

  if (score < 1 || score > 5) {
    errorScore = "Оценка должна быть от 1 до 5";
  }

  if (errorScore) {
    errorNameScore.innerText = errorScore;
    errorNameScore.style.opacity = 1;
  } else {
    errorNameScore.style.opacity = 0;
  }
}

form.addEventListener("submit", handleSubmit);

// Homework-25

let input = document.querySelector(".review__form-name");

function handleSubmit(event) {
  event.preventDefault();

  let name = inputName.value;
  let score = +inputScore.value;

  let errorName = "";

  if (name.length < 2) {
    errorName = "Имя не может быть короче 2-х символов";
  }

  if (name.length === 0) {
    errorName = "Вы забыли указать имя и фамилию";
  }

  if (errorName) {
    errorNameElem.innerText = errorName;
    errorNameElem.style.opacity = 1;
  } else {
    errorNameElem.style.opacity = 0;
  }

  // For Rating

  let errorScore = "";

  if (score < 1 || score > 5) {
    errorScore = "Оценка должна быть от 1 до 5";
  }

  if (errorScore) {
    errorNameScore.innerText = errorScore;
    errorNameScore.style.opacity = 1;
  } else {
    errorNameScore.style.opacity = 0;
  }

  console.log("submit");

  if (!errorName && !errorScore) {
    localStorage.removeItem("name");
  }
}

function handleInput(event) {
  let value = event.target.value;
  let name = event.target.getAttribute("name");

  localStorage.setItem(name, value);
}

input.value = localStorage.getItem("name");

form.addEventListener("submit", handleSubmit, handleFormSubmit);

input.addEventListener("input", handleInput);
