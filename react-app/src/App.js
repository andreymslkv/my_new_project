import Header from "./components/Header/Header";
import PageProduct from "./components/PageProducts/PageProduct";
import Footer from "./components/Footer/Footer";

function App() {
  return (
    <div>
      <Header />
      <PageProduct />
      <Footer />
    </div>
  );
}

export default App;
