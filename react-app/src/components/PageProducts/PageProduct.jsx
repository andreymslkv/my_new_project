import React from "react";

import "./PageProduct.css";

function PageProduct(props) {
  return (
    <div>
      <div className='wrapper'>
        <nav className='breadcrumbs'>
          <div className='breadcrumbs__list'>
            <a className='breadcrumbs__link' href='/'>
              Электроника
            </a>
            <span>&gt;</span>
            <a className='breadcrumbs__link' href='/'>
              Смартфоны и гаджеты
            </a>
            <span>&gt;</span>
            <a className='breadcrumbs__link' href='/'>
              Мобильные телефоны
            </a>
            <span>&gt;</span>
            <a className='breadcrumbs__link' href='/'>
              Apple
            </a>
          </div>
        </nav>

        <main>
          <section className='gallery'>
            <h2 className='title'>Смартфон Apple iPhone 13</h2>

            <ul className='gallery__list'>
              <li className='gallery__item'>
                <img
                  className='gallery__image'
                  src='/images/image-1.webp'
                  alt='Изображение задней и передней части смартфона'
                  height='400'
                  width='304'
                />
              </li>
              <li className='gallery__item'>
                <img
                  className='gallery__image'
                  src='/images/image-2.webp'
                  alt='Изображение передней части смартфона синего цвета'
                  height='400'
                  width='192'
                />
              </li>
              <li className='gallery__item'>
                <img
                  className='gallery__image'
                  src='/images/image-3.webp'
                  alt='Изображение задней и передней части смартфона под углом'
                  height='400'
                  width='199'
                />
              </li>
              <li className='gallery__item'>
                <img
                  className='gallery__image'
                  src='/images/image-4.webp'
                  alt='Изображение трёх задних камер смартфона со встроенной вспышкой'
                  height='400'
                  width='360'
                />
              </li>
              <li className='gallery__item'>
                <img
                  className='gallery__image'
                  src='/images/image-5.webp'
                  alt='Изображение задней части смартфона и половины его передней части'
                  height='400'
                  width='300'
                />
              </li>
            </ul>
          </section>

          <div className='flex-container'>
            <section className='main-info'>
              <h2 className='visually-hidden'>Основная информация</h2>
              <section className='models-colors'>
                <h3 className='section-title'>Цвет товара: cиний</h3>

                <ul className='models-colors__list'>
                  <li className='models-colors__item'>
                    <label>
                      <input
                        className='models-colors__button'
                        type='radio'
                        name='color'
                      />
                      <img
                        className='models-colors__image'
                        src='/images/color-1.webp'
                        alt='Изображение смартфона красного цвета'
                        height='60'
                      />
                    </label>
                  </li>
                  <li className='models-colors__item'>
                    <label>
                      <input
                        className='models-colors__button'
                        type='radio'
                        name='color'
                      />
                      <img
                        className='models-colors__image'
                        src='/images/color-2.webp'
                        alt='Изображение смартфона зелёного цвета'
                        height='60'
                      />
                    </label>
                  </li>
                  <li className='models-colors__item'>
                    <label>
                      <input
                        className='models-colors__button'
                        type='radio'
                        name='color'
                      />
                      <img
                        className='models-colors__image'
                        src='/images/color-3.webp'
                        alt='Изображение смартфона розового цвета'
                        height='60'
                      />
                    </label>
                  </li>
                  <li className='models-colors__item models-colors__item--selected'>
                    <label>
                      <input
                        className='models-colors__button'
                        type='radio'
                        name='color'
                      />
                      <img
                        className='models-colors__image'
                        src='/images/color-4.webp'
                        alt='Изображение смартфона синего цвета'
                        height='60'
                      />
                    </label>
                  </li>
                  <li className='models-colors__item'>
                    <label>
                      <input
                        className='models-colors__button'
                        type='radio'
                        name='color'
                      />
                      <img
                        className='models-colors__image'
                        src='/images/color-5.webp'
                        alt='Изображение смартфона белого цвета'
                        height='60'
                      />
                    </label>
                  </li>
                  <li className='models-colors__item'>
                    <label>
                      <input
                        className='models-colors__button'
                        type='radio'
                        name='color'
                      />
                      <img
                        className='models-colors__image'
                        src='/images/color-6.webp'
                        alt='Изображение смартфона чёрного цвета'
                        height='60'
                      />
                    </label>
                  </li>
                </ul>
              </section>

              <section className='memory-configuration'>
                <h3 className='section-title'>Конфигурация памяти: 128 ГБ</h3>

                <ul className='memory-configuration__list'>
                  <li className='memory-configuration__item memory-configuration__item--selected'>
                    <label>
                      128 ГБ
                      <input
                        className='memory-configuration__button'
                        type='radio'
                        name='configuration'
                      />
                    </label>
                  </li>
                  <li className='memory-configuration__item'>
                    <label>
                      256 ГБ
                      <input
                        className='memory-configuration__button'
                        type='radio'
                        name='configuration'
                      />
                    </label>
                  </li>
                  <li className='memory-configuration__item'>
                    <label>
                      512 ГБ
                      <input
                        className='memory-configuration__button'
                        type='radio'
                        name='configuration'
                      />
                    </label>
                  </li>
                </ul>
              </section>

              <section className='features'>
                <h3 className='section-title'>Характеристики товара</h3>

                <ul className='features__list'>
                  <li className='features__item'>
                    Экран: <b>6.1</b>
                  </li>
                  <li className='features__item'>
                    Встроенная память: <b>128 ГБ</b>
                  </li>
                  <li className='features__item'>
                    Операционная система: <b>iOS 15</b>
                  </li>
                  <li className='features__item'>
                    Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b>
                  </li>
                  <li className='features__item'>
                    Процессор:
                    <a
                      className='features__link_accent'
                      href='https://ru.wikipedia.org/wiki/Apple_A15'
                      target='_blank'
                      rel='noreferrer'
                    >
                      <b>Apple A15 Bionic</b>
                    </a>
                  </li>
                  <li className='features__item'>
                    Вес: <b>173 г</b>
                  </li>
                </ul>
              </section>

              <section className='description'>
                <h3 className='section-title'>Описание</h3>

                <p className='description__text'>
                  Наша самая совершенная система двух камер.
                  <br />
                  Особый взгляд на прочность дисплея.
                  <br />
                  Чип, с которым всё супербыстро.
                  <br />
                  Аккумулятор держится заметно дольше.
                  <br />
                  <em>iPhone 13 - сильный мира всего.</em>
                </p>

                <p className='description__text'>
                  Мы разработали совершенно новую схему расположения и
                  развернули объективы на 45 градусов. Благодаря этому внутри
                  корпуса поместилась наша лучшая система двух камер с
                  увеличенной матрицей широкоугольной камеры. Кроме того, мы
                  освободили место для системы оптической стабилизации
                  изображения сдвигом матрицы. И повысили скорость работы
                  матрицы на сверхширокоугольной камере.
                </p>
                <p className='description__text'>
                  Новая сверхширокоугольная камера видит больше деталей в тёмных
                  областях снимков. Новая широкоугольная камера улавливает на
                  47% больше света для более качественных фотографий и видео.
                  Новая оптическая стабилизация со сдвигом матрицы обеспечит
                  чёткие кадры даже в неустойчивом положении.
                </p>
                <p className='description__text'>
                  Режим «Киноэффект» автоматически добавляет великолепные
                  эффекты перемещения фокуса и изменения резкости. Просто
                  начните запись видео. Режим «Киноэффект» будет удерживать
                  фокус на объекте съёмки, создавая красивый эффект размытия
                  вокруг него. Режим «Киноэффект» распознаёт, когда нужно
                  перевести фокус на другого человека или объект, который
                  появился в кадре. Теперь ваши видео будут смотреться как
                  настоящее кино.
                </p>
              </section>

              <section className='comparison'>
                <h3 className='section__title'>Сравнение моделей</h3>

                <table className='comparison__table'>
                  <thead>
                    <tr>
                      <th className='comparison__tablehead'>Модель</th>
                      <th className='comparison__tablehead'>Вес</th>
                      <th className='comparison__tablehead'>Высота</th>
                      <th className='comparison__tablehead'>Ширина</th>
                      <th className='comparison__tablehead'>Толщина</th>
                      <th className='comparison__tablehead'>Чип</th>
                      <th className='comparison__tablehead'>Объём памяти</th>
                      <th className='comparison__tablehead'>Аккумулятор</th>
                    </tr>
                  </thead>

                  <tbody>
                    <tr className='comparison__row'>
                      <td className='comparison__tablebody-cell'>Iphone11</td>
                      <td className='comparison__tablebody-cell'>194 грамма</td>
                      <td className='comparison__tablebody-cell'>150.9 мм</td>
                      <td className='comparison__tablebody-cell'>75.7 мм</td>
                      <td className='comparison__tablebody-cell'>8.3 мм</td>
                      <td className='comparison__tablebody-cell'>
                        A13 Bionic chip
                      </td>
                      <td className='comparison__tablebody-cell'>до 128 Гб</td>
                      <td className='comparison__tablebody-cell'>
                        До 17 часов
                      </td>
                    </tr>
                    <tr className='comparison__row'>
                      <td className='comparison__tablebody-cell'>Iphone12</td>
                      <td className='comparison__tablebody-cell'>164 грамма</td>
                      <td className='comparison__tablebody-cell'>146.7 мм</td>
                      <td className='comparison__tablebody-cell'>71.5 мм</td>
                      <td className='comparison__tablebody-cell'>7.4 мм</td>
                      <td className='comparison__tablebody-cell'>
                        A14 Bionic chip
                      </td>
                      <td className='comparison__tablebody-cell'>до 256 Гб</td>
                      <td className='comparison__tablebody-cell'>
                        До 19 часов
                      </td>
                    </tr>
                    <tr className='comparison__row'>
                      <td className='comparison__tablebody-cell'>Iphone13</td>
                      <td className='comparison__tablebody-cell'>174 грамма</td>
                      <td className='comparison__tablebody-cell'>146.7 мм</td>
                      <td className='comparison__tablebody-cell'>71.5 мм</td>
                      <td className='comparison__tablebody-cell'>7.65 мм</td>
                      <td className='comparison__tablebody-cell'>
                        A15 Bionic chip
                      </td>
                      <td className='comparison__tablebody-cell'>до 512 Гб</td>
                      <td className='comparison__tablebody-cell'>
                        До 19 часов
                      </td>
                    </tr>
                  </tbody>
                </table>
              </section>
            </section>

            <aside className='sidebar'>
              <section className='price'>
                <h2 className='visually-hidden'>Добавить в корзину</h2>
                <div className='price__top'>
                  <div className='price__top-left'>
                    <span className='price__old'>75 990 &#x20bd;</span>
                    <span className='price__discount'>-8%</span>
                  </div>
                  <button className='price__like'>
                    <svg
                      width='30'
                      height='30'
                      viewBox='0 0 50 50'
                      xmlns='http://www.w3.org/2000/svg'
                    >
                      <path
                        fill-rule='evenodd'
                        clip-rule='evenodd'
                        d='M6.30838 10.9545C10.2979 7.12455 16.7444 7.12455 20.7339 10.9545L25.0002 15.0503L29.2667 10.9545C33.2563 7.12455 39.7027 7.12455 43.6923 10.9545C47.6817 14.7844 47.6817 20.973 43.6923 24.803L25.0002 42.7472L6.30838 24.803C2.31888 20.973 2.31888 14.7844 6.30838 10.9545ZM17.7876 13.7829C15.4253 11.5151 11.617 11.5151 9.25465 13.7829C6.89234 16.0507 6.89234 19.7067 9.25465 21.9746L25.0002 37.0904L40.746 21.9746C43.1083 19.7067 43.1083 16.0507 40.746 13.7829C38.3838 11.5151 34.5754 11.5151 32.2131 13.7829L25.0002 20.7072L17.7876 13.7829Z'
                      />
                    </svg>
                  </button>
                </div>

                <div className='price__new'>67 990&#x20bd;</div>

                <p className='price__delivery'>
                  Самовывоз в четверг, 1 сентября — <b>бесплатно</b>
                </p>
                <p className='price__delivery'>
                  Курьером в четверг, 1 сентября — <b>бесплатно</b>
                </p>

                <button className='button price__button' type='submit'>
                  <img src='/images/basket.svg' alt='Корзина' />
                  <span>Добавить в корзину</span>
                </button>
                <input type='hidden' value='5501' name='product-id' />
              </section>

              <section className='ads-block'>
                <h4 className='ads-block__title'>Реклама</h4>
                <iframe className='frame' src='./ads.html'></iframe>
                <iframe className='frame' src='./ads.html'></iframe>
              </section>
            </aside>
          </div>

          <section className='reviews'>
            <div className='reviews__header'>
              <div className='reviews__count'>
                <h3 className='reviews__title'>Отзывы</h3>
                <span className='reviews__number'>425</span>
              </div>
            </div>

            <div className='review__container'>
              <div className='review__img'>
                <img
                  className='review__photo'
                  src='/images/review-1.jpeg'
                  alt='Фотография мужчины'
                  height='200'
                />
              </div>

              <div className='review__content'>
                <h4 className='review__author'>Марк Г.</h4>

                <div className='review__stars'>
                  <img
                    className='review__star'
                    src='/images/star.svg'
                    alt='Изображение звезды рейтинга золотого цвета'
                    width='30'
                    height='30'
                  />
                  <img
                    className='review__star'
                    src='/images/star.svg'
                    alt='Изображение звезды рейтинга золотого цвета'
                    width='30'
                    height='30'
                  />
                  <img
                    className='review__star'
                    src='/images/star.svg'
                    alt='Изображение звезды рейтинга золотого цвета'
                    width='30'
                    height='30'
                  />
                  <img
                    className='review__star'
                    src='/images/star.svg'
                    alt='Изображение звезды рейтинга золотого цвета'
                    width='30'
                    height='30'
                  />
                  <img
                    className='review__star'
                    src='/images/star.svg'
                    alt='Изображение звезды рейтинга золотого цвета'
                    width='30'
                    height='30'
                  />
                </div>

                <p className='review__experience'>
                  <b>Опыт использования:</b> менее месяца
                </p>
                <p className='review__advantages'>
                  <b>Достоинства:</b>
                  <br />
                  это мой первый айфон после после огромного количества
                  телефонов на андроиде. всё плавно, чётко и красиво. довольно
                  шустрое устройство. камера весьма неплохая, ширик тоже на
                  высоте.
                </p>
                <p className='review__disadvantages'>
                  <b>Недостатки:</b>
                  <br /> к самому устройству мало имеет отношение, но перенос
                  данных с андроида - адская вещь а если нужно переносить фото с
                  компа, то это только через itunes, который урезает качество
                  фотографий исходное
                </p>
              </div>
            </div>

            <div className='review__container'>
              <div className='review__img'>
                <img
                  className='review__photo'
                  src='/images/review-2.jpeg'
                  alt='Фотография мужчины'
                  height='200'
                />
              </div>

              <div className='review__content review__content_last'>
                <h4 className='review__author'>Валерий Коваленко</h4>

                <div className='review__stars'>
                  <img
                    className='review__star'
                    src='/images/star.svg'
                    alt='Изображение звезды рейтинга золотого цвета'
                    width='30'
                    height='30'
                  />
                  <img
                    className='review__star'
                    src='/images/star.svg'
                    alt='Изображение звезды рейтинга золотого цвета'
                    width='30'
                    height='30'
                  />
                  <img
                    className='review__star'
                    src='/images/star.svg'
                    alt='Изображение звезды рейтинга золотого цвета'
                    width='30'
                    height='30'
                  />
                  <img
                    className='review__star'
                    src='/images/star.svg'
                    alt='Изображение звезды рейтинга золотого цвета'
                    width='30'
                    height='30'
                  />
                  <img
                    className='review__star'
                    src='/images/star_gray.svg'
                    alt='Изображение звезды рейтинга золотого цвета'
                    width='30'
                    height='30'
                  />
                </div>

                <p className='review__experience'>
                  <b>Опыт использования:</b> менее месяца
                </p>
                <p className='review__advantages'>
                  <b>Достоинства:</b>
                  <br />
                  OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит
                  долго
                </p>
                <p className='review__disadvantages'>
                  <b>Недостатки:</b>
                  <br />
                  Плохая ремонтопригодность
                </p>
              </div>
            </div>

            <form className='review__form' action='#' method='get'>
              <fieldset className='review__fieldset'>
                <legend className='review__form-legend'>
                  Добавить свой отзыв
                </legend>
                <div className='review__form-top'>
                  <div className='input-name'>
                    <input
                      className='review__form-name'
                      type='text'
                      name='name'
                      placeholder='Имя и фамилия'
                    />
                    <div className='error-name'></div>
                  </div>
                  <div className='input-score'>
                    <input
                      className='review__form-rate'
                      type='number'
                      name='rate'
                      step='1'
                      placeholder='Оценка'
                    />
                    <div className='error-rate'></div>
                  </div>
                </div>
                <textarea
                  className='review__form-text'
                  name='review'
                  cols='30'
                  rows='10'
                  placeholder='Текст отзыва'
                ></textarea>
                <button className='review__button' type='submit'>
                  Отправить отзыв
                </button>
              </fieldset>
            </form>
          </section>
        </main>
      </div>
    </div>
  );
}

export default PageProduct;
