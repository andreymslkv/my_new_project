import React from "react";
import "./Footer.css";

function Footer(props) {
  return (
    <footer className='footer'>
      <div className='wrapper'>
        <div className='footer__flex-container'>
          <p className='footer__info'>
            <span className='footer__copyright'>
              &copy; ООО «<span className='footer_accent'>Мой</span>Маркет»,
              2018-2022.
            </span>
            <span className='footer__phone'>
              Для уточнения информации звоните по номеру
              <a className='footer__link' href='tel:79000000000'>
                +7 900 000 0000
              </a>
              ,
            </span>
            а предложения по сотрудничеству отправляйте на почту
            <a className='footer__link' href='mailto:partner@mymarket.com'>
              partner@mymarket.com
            </a>
          </p>

          <a className='footer__link footer__link_up' href='#header'>
            Наверх
          </a>
        </div>
      </div>
    </footer>
  );
}
export default Footer;
