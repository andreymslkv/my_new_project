"use strict";

const form = document.querySelector("form");
const inputName = document.querySelector(".review__form-name");
const inputNameError = document.querySelector(".error-name");
const inputRating = document.querySelector(".review__form-rate");
const inputRatingError = document.querySelector(".error-rate");
const inputs = form.querySelectorAll("input, textarea");

class Form {
  constructor(input) {
    this.input = input;
    this.value = this.input.replace(/\s+/g, " ").trim();
    this.length = this.value.length;
    this.errorCount = 0;
  }
  get removeErrorName() {
    inputName.classList.remove("error");
    inputNameError.classList.remove("error");
  }
  get removeErrorRating() {
    inputRating.classList.remove("error");
    inputRatingError.classList.remove("error");
  }
  get addErrorName() {
    inputName.classList.add("error");
    inputNameError.classList.add("error");
  }
  get addErrorRating() {
    inputRating.classList.add("error");
    inputRatingError.classList.add("error");
  }
}

class AddReviewForm extends Form {
  constructor(input) {
    super(input);
  }

  get validateRating() {
    if (this.value >= 1 && this.value <= 5) {
      this.removeErrorRating;
      return true;
    } else {
      this.addErrorRating;
      inputRatingError.textContent = "Оценка должна быть от 1 до 5";
    }
    inputRating.addEventListener("input", function () {
      inputRating.classList.remove("error");
      inputRatingError.classList.remove("error");
    });
  }

  get validateName() {
    this.addErrorName;
    if (this.length === 0) {
      inputNameError.textContent = "Вы забыли указать имя и фамилию";
    } else if (this.length < 2) {
      inputNameError.textContent = "Имя не может быть короче 2-х символов";
    } else {
      this.removeErrorName;
      return true;
    }
    inputName.addEventListener("input", function () {
      inputName.classList.remove("error");
      inputNameError.classList.remove("error");
    });
  }

  validate(rule, node, text) {
    if (!rule) {
      node.textContent = text;
    }
  }
}

formInstance = new AddReviewForm();
rules = [
  {
    condition: this.length === 0,
    node: inputName,
    text: "Вы забыли указать имя и фамилию",
  },
  {
    condition: this.length < 2,
    node: inputName,
  },
  {
    condition: this.value >= 1 && this.value <= 5,
    node: inputRating,
  },
];

rules.forEach((r) => {
  formInstance.validate(r.condition, r.node);
});
// formInstance.validate(this.length === 0, inputName)
// formInstance.validate(this.length < 2, inputName)
// formInstance.validate(this.value >= 1 && this.value <= 5, inputRating)

form.addEventListener("submit", function (evt) {
  evt.preventDefault();
  for (rule of rules) {
    formInstance.validate(rule.condition, rule.node, rule.text);

    if (!rule.condition) {
      break;
    }
  }
  let valName = new AddReviewForm(inputName.value);
  let valRating = new AddReviewForm(inputRating.value);
  valName.validateName;
  valRating.validateRating;
  if (inputNameError.classList.contains("error")) {
    inputRatingError.classList.remove("error");
  }
  if (valName.validateName && valRating.validateRating) {
    inputs[0].value = "";
    inputs[1].value = "";
    inputs[2].value = "";
    localStorage.clear();
  }
});

inputs.forEach(function (input) {
  input.value = localStorage.getItem(input.name);
  input.addEventListener("input", function () {
    localStorage.setItem(input.name, input.value);
  });
});
