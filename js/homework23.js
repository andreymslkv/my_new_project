"use strict";

// Упражнение 1

let answer = +prompt("Введите число");
let count = answer;

let intervalId = setInterval(function () {
  if (count === 0) {
    clearInterval(intervalId);
    console.log("Время вышло!");
  } else if (!count) {
    clearInterval(intervalId);
    console.log("Ошибка! Тут должно быть число!");
  } else {
    console.log("Осталось " + count);
  }
  count--;
}, 1000);

// Упражнение 2

let promise = fetch("https://reqres.in/api/users");
let time = performance.now();

promise
  .then(function (response) {
    return response.json();
  })
  .then(function (response) {
    let users = response.data;

    console.log(`Получили пользователей: ${users.length}`);

    users.forEach(function (user) {
      console.log(` - ${user.first_name} ${user.last_name} (${user.email})`);
    });
  })
  .catch(function () {
    console.log("Ошибка");
  });

console.log(`Время выполнения в миллисекундах: ${Math.round(time)}`);
