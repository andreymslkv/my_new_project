"use strict";

// Упражнение 1

let obj = {};

/**
 * Возвращает true, если объект пустой иначе false
 * @param {любой параметр} obj
 * @returns {true или false}
 */

function isEmpty(obj) {
  for (let key in obj) {
    return false;
  }
  return true;
}

console.log(isEmpty(obj));

// Упражнение 2 выполнено в отдельном файле data.js

// Упражнение 3

let salaries = {
  John: 100000,
  Ann: 160000,
  Pete: 130000,
};

let newSalaries = {};

/**
 * Функция расчитывает повышение зарплаты на указанный процент и возвращает новое значение
 * @param {any} percent процент
 * @returns {any} возврат нового значения
 */
function raiseSalary(percent) {
  let newSalaries = {};

  for (let key in salaries) {
    let raise = (salaries[key] * percent) / 100;

    newSalaries[key] = salaries[key] + raise;
  }
  return newSalaries;
}

/**
 * Расчёт общего бюджета зарплат
 * @param {any} obj зарплаты каждого сотрудника
 * @returns {any} возвращает сумму всех зарплат
 */
function calcSalaryBudget(obj) {
  let salaryBudget = 0;

  for (let key in obj) {
    salaryBudget += obj[key];
  }

  return salaryBudget;
}

let result = raiseSalary(5);

let salaryBudget = calcSalaryBudget(result);

console.log(salaries, result);

console.log(Math.floor(136.5));

console.log(salaryBudget);
