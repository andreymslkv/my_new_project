"use strict";

// Упражнение 1

function getSum(arr) {
  let newArr = arr.filter((value) => typeof value === "number");

  let sum = 0;

  for (let i = 0; i < newArr.length; i += 1) {
    sum += newArr[i];
  }

  return sum;
}

console.log(getSum([15, 10, 10, 20, "ddd", 100]));

// Упражнение 2 выполнено в отдельном файле data.js

// Упражнение 3

let cart = [4884];

function addToCart(productId) {
  let hasInCart = cart.includes(productId);

  if (hasInCart) return;

  cart.push(productId);
}

function removeFromCart(productId) {
  cart = cart.filter(function (id) {
    return id !== productId;
  });
}

// Добавили товар

addToCart(3456);

console.log(cart); // [4884, 3456]

// Удалили товар

removeFromCart(4884);

console.log(cart); // [3456]
