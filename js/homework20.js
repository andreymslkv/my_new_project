"use strict";

// Упражнение 1

for (let n = 0; n <= 10; n++) {
  if (n % 2 === 0) {
    console.log(n);
  }
}

// Упражнение 2

let count = 0;

for (let i = 0; i < 3; i++) {
  let number = +prompt("Введите число", "");
  if (Number.isNaN(number)) {
    alert("Ошибка, вы ввели не число");
    break;
  } else {
    count = number + count;
  }
  if (i === 2) {
    alert(count);
  }
}

// Упражнение 3

function getNameOfMonth(i) {
  if (i === 1) return "Январь";
  if (i === 2) return "Февраль";
  if (i === 3) return "Март";
  if (i === 4) return "Апрель";
  if (i === 5) return "Май";
  if (i === 6) return "Июнь";
  if (i === 7) return "Июль";
  if (i === 8) return "Август";
  if (i === 9) return "Сентябрь";
  if (i === 10) return "Октябрь";
  if (i === 11) return "Ноябрь";
  if (i === 12) return "Декабрь";
}

for (let i = 1; i <= 12; i++) {
  const month = getNameOfMonth(i);

  if (i === 10) continue;

  console.log(month);
}

// Упражнение 4

/**
 * Выражение функции, вызываемое немедленно (IIFE), - это выражение функции, которое вызывает себя автоматически. Иногда мы называем IIFE "выражением самозапускающейся функции" или "выражением самоисполняющейся анонимной функции".
 * Например:
 * (function addNumbers() {
  console.log(100 + 20);
})();
 */
