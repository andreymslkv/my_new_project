"use strict";

// Упражнение 1

let a = "$100";
let b = "300$";

let sum = Number(a.replace("$", " ")) + Number(b.replace("$", " "));

console.log(sum);

// Упражнение 2

let message = " привет, медвед      ";

message = message.trim();
message = message[0].toUpperCase() + message.slice(1);

console.log(message); // “Привет, медвед”

// Упражнение 3

let age = prompt("Сколько вам лет?");

if (age <= 3) {
  let message = `Вам ${age} лет и вы младенец`;
  console.log(message);
} else if (age <= 11) {
  let message = `Вам ${age} лет и вы ребёнок`;
  console.log(message);
} else if (age <= 18) {
  let message = `Вам ${age} лет и вы подросток`;
  console.log(message);
} else if (age <= 40) {
  let message = `Вам ${age} лет и вы познаёте жизнь`;
  console.log(message);
} else if (age <= 80) {
  let message = `Вам ${age} лет и вы познали жизнь`;
  console.log(message);
} else if (age >= 81) {
  let message = `Вам ${age} лет и вы долгожитель`;
  console.log(message);
} else {
  let message = "Ошибка!";
  console.log(message);
}

// Упражнение 4

let message2 = "Я работаю со строками как профессионал!";
let count = message2.split(" ");

console.log(count.length);
